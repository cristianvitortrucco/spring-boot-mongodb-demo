# Descrição do Projeto
Esta é uma Demo simples de usar `Spring Boot` e` MongoDB`. O aplicativo tem um formulário simples na página principal.
Os dados inseridos no formulário serão guardados na coleção 'usuários' no `MongoDB`.


# Como executar no Docker
Esta seção descreve como executar este aplicativo e `MongoDB` em contêiner Docker.
O Docker já deve estar instalado.

### Criar rede
`` `Bash
A rede docker cria spring_demo_net
`` `

### Executar MongoDB no Docker
Criar pasta para armazenar dados do Mongo DB:
`` `Bash
Mkdir -p ~ / mongo-data
`` `

Executar o Mongo no recipiente:
Antes de iniciar o `MongoDB` no Docker, certifique-se de que o Mongo não seja iniciado no host onde o recipiente será iniciado.
`` `Bash
Docker run --name spring-demo-mongo --network = spring_demo_net -v / home / ubuntu / mongo-data: / data / db -d mongo
`` `

### Crie projeto e copie o arquivo jar para a pasta docker
Para construir o projeto atual e copiar o arquivo `jar` para a pasta` docker` do projeto atual:
`` `Bash
./gradlew clean build && cp build / libs / springboot-mongo-demo.jar docker /
`` `
A pasta `docker` contém um` Dockerfile` a partir do qual a imagem do aplicativo será criada

### Crie a imagem do aplicativo
Vá para a pasta `docker` deste projeto e crie imagem:
`` `Bash
Cd docker & & docker build --tag = spring-demo-1.0.
`` `

### Run Spring Boot no Docker
Para executar o arquivo de compilação `jar` no Docker:
`` `Bash
Docker run -d --name spring-demo --network = spring_demo_net -p 8080: 8080 spring-demo-1.0
`` `

### Verifique o aplicativo e o Mongo começou corretamente
Abra [http: // localhost: 8080] (http: // localhost: 8080) em qualquer navegador, adicione novo usuário e verifique se o novo usuário é exibido
Na tabela `Saved Users '.

Http://spring-mongo.vertigo.com.br/api/users/

Para verificar os logs de contêineres do `MongoDB`:
`` `Bash
Logs docker spring-demo-mongo
`` `

Para verificar os logs do Spring Boot Application:
`` `Bash
Logs docker spring-demo
`` `


## Arquitetura do projeto
O aplicativo consiste em:
* `UserController` que lida com a solicitação da página `index.html`
* `UserResource` - é` REST` ponto final para recuperar `Users`
* `UserRepository` - interface de repositório para persistência e recuperação de 'Usuários' de` MongoDB`

Porque é uma demo simples para armazenar e recuperar usuários de / para banco de dados, 'UserRepository` injetado diretamente em
`UserController` e` UserResource`.
Google Tradutor para empresas:Google Toolkit de tradução para appsTradutor de sites